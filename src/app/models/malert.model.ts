export interface Malert {
	Criteria: {
	ID: string;
	SourceAssetCriticality: string;
	TargetAssetCriticality: string;
	TimeToDueDate: string;
	TimeFromDetectTime: string;
	SeverityForAttackCategory: string;
	MAQuality: string;
	AttackSourceReputationScore: string;
	MaxCVE: string;
	Priority: string;};
	MetaAlert: {
	_id: string;
	ID: string;
	DetectTime: string;
	MACategory: string;
	AggrID: string;
	TopAssetCriticality: string;
	TopMissionImportance: string;
	MAQuality: string;
	Source: {
		Asset: string;
		MaxAssetCriticality: string;
		Mission: string;
		MaxMissionImportance: string;
		IP4: string;
		Proto: string;
		Port: string;
		Hostname: string;
	};
	Target: {
		Asset: string;
		MaxAssetCriticality: string;
		Mission: string;
		MaxMissionImportance: string;
		IP4: string;
		Proto: string;
		Port: string;
		Hostname: string;
	}
 }
}

