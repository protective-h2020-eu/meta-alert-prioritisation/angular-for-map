import { Injectable }   from '@angular/core';
import { HttpClient }   from '@angular/common/http';
import { Observable }   from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import { Malert } from '../models/malert.model';

@Injectable()
export class MalertService {

  private serviceUrl = 'http://localhost:3000/rank-meta-alert';
  
  constructor(private http: HttpClient) { }
  
  getMalert(): Observable<Malert[]> {
    var object=this.http.get<Malert[]>(this.serviceUrl);
	console.log("This");
	return object;
  }
  
}
