import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MalerttableComponent } from './malerttable.component';

describe('MalerttableComponent', () => {
  let component: MalerttableComponent;
  let fixture: ComponentFixture<MalerttableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MalerttableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MalerttableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
