import { Component, OnInit} from '@angular/core';
import { MalertService } from '../../services/malert.service';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';
import {DataSource} from '@angular/cdk/collections';
import { Malert } from '../../models/malert.model';

@Component({
  selector: 'malerttable',
  templateUrl: './malerttable.component.html',
  styleUrls: ['./malerttable.component.css']
})
export class MalerttableComponent implements OnInit {
  dataSource = new MalertDataSource(this.malertService);
  displayedColumns = ['priority', 'DetectTime', 'id', 'MAQuality', 'srcip'];
  constructor(private malertService: MalertService) { }
    
  ngOnInit() { 
  }
}

export class MalertDataSource extends DataSource<any> {
  constructor(private malertService: MalertService) {
    super();
  }
  connect(): Observable<Malert[]> {
    return this.malertService.getMalert();
  }
  disconnect() {}
}
